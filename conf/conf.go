package conf

import (
	"encoding/json"
	"io/ioutil"
)

type Configuration struct {
	Server   string `json:"server"`
	Port     int    `json:"port"`
	User     string `json:"user"`
	Password string `json:"password"`
	Listen   struct {
		Ip   string `json:"ip"`
		Port string `json:"port"`
	}
	Log string `json:"log"`
}

func Read(params ...string) (conf *Configuration, err error) {
	file_name := "/etc/gomail/config.json"
	if len(params) > 0 {
		file_name = params[0]
	}
	bytes, err := ioutil.ReadFile(file_name)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(bytes, &conf)
	if err != nil {
		return nil, err
	}
	return conf, nil
}
