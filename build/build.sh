#!/usr/bin/env bash

NAME="gomail"
VERSION="0.0.4"
COMPANY="serg"
EMAIL="serg.soloviev@gmail.com"
DESCRIPTION="json to smtp"
APT_SERVER=""
SERVICE_HOST="example.com"
SERVICE_IP="127.0.0.1"
SERVICE_PORT="8888"

#-------------------------------------------------------------------------------
cd ../
echo "building:"
GOOS=linux GOARCH=amd64 go build -v -o build/linux/$NAME main.go
# -ldflags="-s -w"
# upx --brute $NAME
#-------------------------------------------------------------------------------
echo "deb package dir..."
cd build/
rm $NAME*.deb
rm -rf debian
mkdir -p debian/DEBIAN
mkdir -p debian/var/log/$NAME
mkdir -p debian/etc/$NAME
mkdir -p debian/etc/nginx/conf.d
mkdir -p debian/usr/local/$NAME/bin
mkdir -p debian/lib/systemd/system
cp linux/$NAME debian/usr/local/$NAME/bin
touch debian/var/log/$NAME/$NAME.log
#-------------------------------------------------------------------------------
echo "service conf files:"
echo -e "\t/DEBIAN/control"
echo "Package: $NAME
Version: $VERSION
Maintainer: $COMPANY <$EMAIL>
Architecture: all
Description: $DESCRIPTION
" >> debian/DEBIAN/control

echo -e "\t/DEBIAN/postinst"
echo "#!/bin/sh

echo \"starting $NAME...\n\"
sudo service $NAME start
" >> debian/DEBIAN/postinst

echo -e "\t/DEBIAN/prerm"
echo "#!/bin/sh

echo \"stoping $NAME...\n\"
sudo service $NAME stop
" >> debian/DEBIAN/prerm

echo -e "\t/lib/systemd/system/$NAME.service"
echo "[Unit]
Description=$NAME service
After=network.target

[Service]
Type=simple
ExecStart=/usr/local/$NAME/bin/$NAME
KillMode=process
Restart=on-failure
RestartSec=15s

[Install]
WantedBy=multi-user.target
" >> debian/lib/systemd/system/$NAME.service

echo -e "\t/etc/$NAME/config.json"
echo '{
    "listen": {
        "ip": "'$SERVICE_IP'",
        "port": "'$SERVICE_PORT'"
    },
    "log": "/var/log/'$NAME'/'$NAME'.log",
    "server": "smtp.example.com",
    "port": 587,
    "user": "user@example.com",
    "password": "password"
}
' >> debian/etc/$NAME/config.json

echo "upstream $NAME-service {
    server $SERVICE_IP:$SERVICE_PORT fail_timeout=0;
}
server {
    listen       80;
    server_name  $SERVICE_HOST;
    charset      utf-8;
    set \$project $NAME;
    sendfile on;
    location / {
        proxy_pass http://$NAME-service;
        proxy_set_header Host \$host;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Protocol \$scheme;
        proxy_connect_timeout 30s;
        proxy_read_timeout 30s;
        proxy_redirect off;
    }
}
" >> debian/etc/nginx/conf.d/$SERVICE_HOST.nginx.conf

#-------------------------------------------------------------------------------
chmod 755 debian/DEBIAN/postinst
chmod 755 debian/DEBIAN/prerm
#-------------------------------------------------------------------------------

echo "building deb package"
fakeroot dpkg-deb --build debian .
#dpkg-deb --build debian .

echo "move deb package to apt repo..."
# scp $NAME*.deb $APT_SERVER:/var/www/debrepo/dist
cp $NAME*.deb /var/www/debrepo/dist

echo "cleaning..."
rm $NAME*.deb
rm -rf debian/
rm -rf linux/
