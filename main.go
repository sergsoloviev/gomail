package main

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"net/smtp"
	"os"
	"regexp"

	"bitbucket.org/sergsoloviev/gomail/conf"
)

type Message struct {
	To      string `json:"to"`
	Subject string `json:"subject"`
	Message string `json:"message"`
}

var (
	Conf conf.Configuration
)

func send_mail(c *conf.Configuration, to string, title string, body string) {
	auth := smtp.PlainAuth(
		"",
		c.User,
		c.Password,
		c.Server,
	)
	addr := fmt.Sprintf("%s:%v", c.Server, c.Port)
	from := c.User

	header := make(map[string]string)
	header["From"] = from
	header["To"] = to
	header["Subject"] = title
	header["MIME-Version"] = "1.0"
	header["Content-Type"] = "text/plain; charset=\"utf-8\""
	header["Content-Transfer-Encoding"] = "base64"

	message := ""
	for k, v := range header {
		message += fmt.Sprintf("%s: %s\r\n", k, v)
	}
	message += "\r\n" + base64.StdEncoding.EncodeToString([]byte(body))

	err := smtp.SendMail(addr, auth, from, []string{to}, []byte(message))
	if err != nil {
		log.Println(err)
	}
}

func decode_json(r *http.Request) (Message, error) {
	// decode
	decoder := json.NewDecoder(r.Body)
	mail_message := Message{}
	err := decoder.Decode(&mail_message)
	if err != nil {
		log.Println("error: ", err)
	}
	//validate
	if len(mail_message.To) == 0 {
		err = errors.New("'To' field is zero")
		log.Println("error:", err)
	}
	if len(mail_message.Subject) == 0 {
		err = errors.New("'Subject' field is zero")
		log.Println("error:", err)
	}

	if len(mail_message.Message) == 0 {
		err = errors.New("'Message' field is zero")
		log.Println("error:", err)
	}
	re := regexp.MustCompile(".+@.+\\..+")
	matched := re.Match([]byte(mail_message.To))
	if !matched {
		err = errors.New("'To' field is invalid")
		log.Println("error:", err)
	}
	return mail_message, err
}

func mail_handler(w http.ResponseWriter, r *http.Request, c *conf.Configuration) {
	message, err := decode_json(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	} else {
		go send_mail(c, message.To, message.Subject, message.Message)
		log.Println("mail: " + message.To)
	}
}

func main() {
	Conf, err := conf.Read()
	if err != nil {
		panic(err)
	}
	logfile, err := os.OpenFile(Conf.Log, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		panic(err)
	}
	defer logfile.Close()
	log.SetOutput(logfile)

	listen := fmt.Sprintf("%s:%s", Conf.Listen.Ip, Conf.Listen.Port)

	http.HandleFunc("/mail", func(w http.ResponseWriter, r *http.Request) {
		mail_handler(w, r, Conf)
	})
	log.Println("http://" + listen)
	http.ListenAndServe(listen, nil)
}
